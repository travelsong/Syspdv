#!/bin/bash

mv /etc/apt/sources.list /etc/apt/sources.list.old

echo "###### Ubuntu Main Repos" > /etc/apt/sources.list
echo "deb http://archive.ubuntu.com/ubuntu/ precise main restricted universe multiverse " >> /etc/apt/sources.list
echo "deb-src http://archive.ubuntu.com/ubuntu/ precise main restricted universe multiverse " >> /etc/apt/sources.list
echo "" >> /etc/apt/sources.list
echo "###### Ubuntu Update Repos" >> /etc/apt/sources.list
echo "deb http://archive.ubuntu.com/ubuntu/ precise-security main restricted universe multiverse " >> /etc/apt/sources.list
echo "deb http://archive.ubuntu.com/ubuntu/ precise-updates main restricted universe multiverse " >> /etc/apt/sources.list
echo "deb http://archive.ubuntu.com/ubuntu/ precise-proposed main restricted universe multiverse " >> /etc/apt/sources.list
echo "deb http://archive.ubuntu.com/ubuntu/ precise-backports main restricted universe multiverse " >> /etc/apt/sources.list
echo "deb-src http://archive.ubuntu.com/ubuntu/ precise-security main restricted universe multiverse " >> /etc/apt/sources.list
echo "deb-src http://archive.ubuntu.com/ubuntu/ precise-updates main restricted universe multiverse " >> /etc/apt/sources.list
echo "deb-src http://archive.ubuntu.com/ubuntu/ precise-proposed main restricted universe multiverse " >> /etc/apt/sources.list
echo "deb-src http://archive.ubuntu.com/ubuntu/ precise-backports main restricted universe multiverse " >> /etc/apt/sources.list
echo "" >> /etc/apt/sources.list
echo "###### Ubuntu Partner Repo" >> /etc/apt/sources.list
echo "deb http://archive.canonical.com/ubuntu precise partner" >> /etc/apt/sources.list
echo "deb-src http://archive.canonical.com/ubuntu precise partner" >> /etc/apt/sources.list
echo "" >> /etc/apt/sources.list
echo "###### Ubuntu Extras Repo" >> /etc/apt/sources.list
echo "deb http://extras.ubuntu.com/ubuntu precise main" >> /etc/apt/sources.list
echo "deb-src http://extras.ubuntu.com/ubuntu precise main" >> /etc/apt/sources.list

apt-get update

apt-get remove -y wine*

apt-get autoremove -y

tar -xvzf update_wine.tar.gz

dpkg -i archives/*.deb

cp syspdv /usr/local/bin
